import asyncio
import os
import aiohttp
import aiofiles

async def main():
    subreddit = input('Subreddit: ')

    if not os.path.exists(subreddit):
        os.makedirs(subreddit)

    before = 't3_cdub3j'
    count = 0
    num = 0
    
    async def api_call(subreddit, before, str: count):
        async with aiohttp.request("GET", f"https://www.reddit.com/r/{subreddit}/hot.json?limit=100&after={before}&count={count}", headers={'User-Agent' : "Magic Browser"}) as url:
            data = await url.json()
            return data
    
    posts = await api_call(subreddit, before, count)
    before = posts['data']['after']
    
    while posts['data']['children'] is not []:
        #count += len(posts['data']['children'])
        count += 1
        before = posts['data']['after']

        for post in posts['data']['children']:
            pick = post['data']['url']
            filename = f'{subreddit}/{subreddit}-{pick.split("/")[-1]}'
            filename2 = f'{subreddit}-{pick.split("/")[-1]}'

            if len(filename2) >= 50:
                print('too long')
            else:
                try:
                    if post['data']['post_hint'] == 'image':
                        if filename2 not in os.listdir(subreddit):
                            async with aiohttp.request("GET", pick, headers={'User-Agent' : "Magic Browser"}) as url:
                                if url.status == 200:
                                    f = await aiofiles.open(filename, mode='wb')
                                    await f.write(await url.read())
                                    await f.close()
                                    
                                    print(f'Downloaded {num}: {filename}')
                                    num += 1
                        else:
                            print('duplicated image')
                    else:
                        pass
                except KeyError:
                    pass


        print(count)
        posts = await api_call(subreddit, before, count)
        await asyncio.sleep(5)

asyncio.run(main())

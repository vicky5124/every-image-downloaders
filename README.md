# Every Image Downloaders

Small scripts that download every image from different sites.

### Dependencies:
- Aiohttp `python -m pip install aiohttp --user`
- Asyncio `included with Python, requires an implementation only available on 3.7`
- Aiofiles `python -m pip install aiofiles --user`
- Python 3.7, does not work with python 3.6 or older.

### Catches:
- Reddit post limit is 1000
- Sankaku API Is SLOW


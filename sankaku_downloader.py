import asyncio
import os
import aiohttp
import aiofiles
import sys

async def main():
    if sys.argv[1:] == []:
        tags = input('Tags: ')
    else:
        print(sys.argv[1:])
        tagsw = ''
        for item in sys.argv[1:]:
            tagsw += item
            tagsw += ' '
        tags = tagsw.rstrip()

    if not os.path.exists(tags):
        os.makedirs(tags)
    
    async def api_call(tags, page=0):
        async with aiohttp.request("GET", f"https://capi-v2.sankakucomplex.com/posts?lang=english&tags={tags}&page={page}&limit=100", headers={'User-Agent' : "Magic Browser"}) as url:
            data = await url.json()
            return data

    async def download_post(pick, filename, filename2, num):
        if len(filename2) >= 100:
            print('Filename too long')
        else:
            try:
                if filename2 not in os.listdir(tags):
                    async with aiohttp.request("GET", pick, headers={'User-Agent' : "Magic Browser"}) as url:
                        if url.status == 200:
                            f = await aiofiles.open(filename, mode='wb')
                            await f.write(await url.read())
                            await f.close()

                            print(f'Downloaded {num}: {filename}')
                else:
                    print('duplicate image')
            except KeyError:
                pass



    posts = await api_call(tags)
    print(len(posts))
    x = num = 1
    if len(posts) != 0:
        y = 100
    else:
        print('One of the tags doesn\'t exist')
        return

    while y is 100:
        posts = await api_call(tags, x)
        for post in posts:
            #await asyncio.sleep(1)
            pick = post['file_url']

            #filename = f'{tags}/{tags}-{pick.split("/")[-1]}'
            filename = f'{tags}/{pick.split("/")[-1]}'
            filename = filename.split('?', 1)[0]
            #filename2 = f'{tags}-{pick.split("/")[-1]}'
            filename2 = f'{pick.split("/")[-1]}'
            filename2 = filename2.split('?', 1)[0]

            while True:
                try:
                    await download_post(pick, filename, filename2, num)
                except:
                    await asyncio.sleep(10)
                    continue
                break

            num += 1
        await asyncio.sleep(10)

        print(len(posts))
        y = len(posts)
        x += 1

asyncio.run(main())
